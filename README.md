# Problem #2 Solution
Authored by Lachlan du Preez

## Usage
Requires Node.js ^6.0.0.

Install node modules by running `npm i`. Once installed, run `npm test` to view the result.

The business logic lies in the `pie.js` file found in the src directory, with tests in its directory.

## Further considerations
The current example is limited. The method only supports one type of input (as per the example). The current format works for bulk checks which doesn't support other data types. I suggest abstracting the input preparation to satisfy a factory or adapter pattern. This will remedy the bulky code and resolve close bundling of conditional statement checks and enable more factory methods for other input types. 

Moving the method to a class will enable the development of unit tests, especially for the math heavy closure `pointFallsInsideProgressPie`.
