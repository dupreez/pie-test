const assert = require('assert');
const fs = require('fs');
const path = require('path');
const { pie } = require('../src/pie');

describe('Progress Pie', function() {
  it('Sample Input', function() {
    const input = fs.readFileSync(path.join(__dirname, 'input.txt'), 'utf8');
    const output = fs.readFileSync(path.join(__dirname, 'output.txt'), 'utf8');

    assert.equal(pie(input), output);
  });
});
