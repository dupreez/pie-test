exports.pie = (input) => {
  if (!input) {
    throw new Error('input is missing');
  }

  const lines = input.split(/[\r\n]+/).filter((l) => !!l);
  const count = parseInt(lines.shift());

  if (count !== lines.length) {
    throw new Error('number of inputs does not match number of lines');
  }

  if (count < 1) {
    throw new Error('number of points cannot be nil')
  }

  if (count > 1000) {
    throw new Error('number of points cannot be greater than 1000')
  }

  const readLine = (line) => {
    let matches = line.match(/(\d+) (\d+) (\d+)/);

    return {
      p: parseInt(matches[1]),
      x: parseInt(matches[2]),
      y: parseInt(matches[3]),
    };
  };

  const pointFallsInsideProgressPie = (p, x, y) => {
    const radius = 50;
    const circle = {
      x: 50,
      y: 50,
    };

    // Find if the point lies inside the circle (ignoring progress)
    let insideCircle = ((x - circle.x) ^ 2) + ((y - circle.y) ^ 2) <= (radius ^ 2);

    // Find if the progress' angle is greater than the point's angle
    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/atan2
    let changeInY = y - circle.y;
    let changeInX = x - circle.x;
    let pointDegrees = Math.atan2(changeInY, changeInX) * 180 / Math.PI;
    let pointAngle = (pointDegrees + 360) % 360;
    let progressAngle = (p / 100) * 360;
    let insideProgress = progressAngle > pointAngle;

    return insideCircle && insideProgress;
  };

  let result = '';

  // Each loop appends case colour to result
  lines.forEach((line, index) => {
    let { p, x, y } = readLine(line);
    let caseNum = index + 1;

    if (p < 0 || p > 100) {
      throw new Error(`Case ${caseNum}: progress must fall between 0 and 100.`);
    }

    if (x < 0 || x > 100) {
      throw new Error(`Case ${caseNum}: coordinate X must fall between 0 and 100.`);
    }

    if (y < 0 || y > 100) {
      throw new Error(`Case ${caseNum}: coordinate Y must fall between 0 and 100.`);
    }

    let inside = pointFallsInsideProgressPie(p, x, y);

    result += `Case #${caseNum}: ${inside ? 'black' : 'white'}\n`;
  });

  return result;
};
